module.exports = function(grunt) {

    grunt.initConfig({
        less: {
            dev: {
                options: {
                    compress: true
                },
                files: {
                    'assets/css/main.css': ['assets/less/main.less']
                }
            }
        },
        watch: {
            files: ['assets/less/**/*'],
            tasks: ['less']
        }
    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', [
        'less', 
        'watch'
    ]);
};