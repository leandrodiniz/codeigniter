<?php 
$iduser = $this->uri->segment(3);

if(null == $iduser) redirect ('home/view');

$query = $this->crud->get_byid($iduser)->row();

echo '<h2>Remove user</h2>';

echo form_open("home/remove/$iduser");

echo form_label('Name:');
echo form_input(array('name'=>'name'), set_value('name', $query->name), 'disabled="disabled"');


$options = array(
    ''  => 'Select',
    'admin'  => 'Administrator',
    'editor'    => 'Editor',
    'contibutor'   => 'Contibutor',
    'author' => 'Author',
);

echo form_label('Category:');
echo form_dropdown('category', $options, set_value('category', $query->category), 'disabled="disabled"');

echo form_label('Date:');
echo form_input(array('name'=>'date'), set_value('date', $query->date), 'disabled="disabled"');

echo '<p class="delete-message">Are you sure you want to delete this user?</p>';
echo form_submit(array('name'=>'Delete'),'Delete');

echo form_hidden('id_user', $query->id);

echo form_close();