<?php 
echo '<h2>Edit a user</h2>';

$iduser = $this->uri->segment(3);

if(null == $iduser) redirect ('home/view');

$query = $this->crud->get_byid($iduser)->row();

echo form_open("home/edit/$iduser");

echo form_label('Name:');
echo form_input(array('name'=>'name'), set_value('name', $query->name));


$options = array(
    ''  => 'Select',
    'admin'  => 'Administrator',
    'editor'    => 'Editor',
    'contibutor'   => 'Contibutor',
    'author' => 'Author',
);

echo form_label('Category:');
echo form_dropdown('category', $options, set_value('category', $query->category), '');

echo form_label('Date:');
echo form_input(array('name'=>'date'), set_value('date', $query->date));

//error message
echo validation_errors('<p class="error-message">', '</p>');

//success message
if($this->session->flashdata('success_edit')) :
    echo '<p class="success-message">'. $this->session->flashdata('success_edit') . '</p>';
endif;

echo form_submit(array('name'=>'Edit'),'Edit');

echo form_hidden('id_user', $query->id);

echo form_close();