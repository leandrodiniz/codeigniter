<?php 
echo '<h2>Add new user</h2>';

echo form_open('home/add');

echo form_label('Name:');
echo form_input(array('name'=>'name'), set_value('name'));


$options = array(
    ''  => 'Select',
    'admin'  => 'Administrator',
    'editor'    => 'Editor',
    'contibutor'   => 'Contibutor',
    'author' => 'Author',
);

echo form_label('Category:');
echo form_dropdown('category', $options, set_value('category'),'');

echo form_label('Date:');
echo form_input(array('name'=>'date'), set_value('date'));

//error message
echo validation_errors('<p class="error-message">', '</p>');

//success message
if($this->session->flashdata('success')) :
    echo '<p class="success-message">'. $this->session->flashdata('success') . '</p>';
endif;

echo form_submit(array('name'=>'Send'),'Send');

echo form_close();