<?php

echo '<h2>Registered user</h2>';

//success message
if($this->session->flashdata('success_remove')) :
    echo '<p class="success-message">'. $this->session->flashdata('success_remove') . '</p>';
endif;

$this->table->set_heading('ID', 'Name', 'Category', 'Date', 'Action');
foreach ($users as $user) :
	$this->table->add_row($user->id, $user->name, $user->category, $user->date, anchor("home/edit/$user->id", 'Edit', array('class' => 'edit-user link-action')).' - ' . anchor("home/remove/$user->id", 'Remove', array('class' => 'remove-user link-action')));
endforeach;

echo $this->table->generate();