<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>CodeIgniter</title>

	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url('assets/css/main.css'); ?>" type="text/css" rel="stylesheet" />
</head>
<body>
	<div class="container">
		<header>
			<nav>
				<ul>
					<li><?php echo anchor('home/add', 'Add new user') ?></li>
					<li><?php echo anchor('home/view', 'Registered users') ?></li>
				</ul>
			</nav>
		</header>
		<main>
			<?php if($page!='') $this->load->view('pages/' .$page); ?>
		</main>
	</div>
	<script src="<?php echo base_url('assets/vendors/jquery/jquery.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/vendors/jquery.maskedinput/dist/jquery.maskedinput.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/main.js'); ?>"></script>
</body>
</html>