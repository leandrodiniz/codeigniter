<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('crud_model', 'crud');
    }

    public function index()
    {
        $data = array(
            'page' => '',
        );

        $this->load->view('home', $data);
    }

    public function add()
    {
        //form validation
        $this->form_validation->set_rules('name', 'NAME', 'trim|required');
        $this->form_validation->set_rules('category', 'CATEGORY', 'trim|required');
        $this->form_validation->set_rules('date', 'DATE', 'trim|required');

        if(true == $this->form_validation->run()) :
            $data = elements(array('name', 'category', 'date'), $this->input->post());
            $this->load->model('crud_model');
            $this->crud->do_insert($data);
        endif;

        $data = array(
            'page' => 'add',
        );

        $this->load->view('home', $data);
    }

    public function view()
    {
        $data = array(
            'page' => 'view',
            'users' => $this->crud->get_users()->result(),
        );

        $this->load->view('home', $data);
    }

    public function edit()
    {
        //form validation
        $this->form_validation->set_rules('name', 'NAME', 'trim|required');
        $this->form_validation->set_rules('category', 'CATEGORY', 'trim|required');
        $this->form_validation->set_rules('date', 'DATE', 'trim|required');

        if(true == $this->form_validation->run()) :
            $data = elements(array('name', 'category', 'date'), $this->input->post());
            $this->crud->do_update($data, array('id' => $this->input->post('id_user')));
        endif;

        $data = array(
            'page' => 'edit',
        );

        $this->load->view('home', $data);
    }

    public function remove()
    {
        if($this->input->post('id_user') > 0) :
            $this->crud->do_delete(array('id' => $this->input->post('id_user')));
        endif;

        $data = array(
            'page' => 'remove',
        );

        $this->load->view('home', $data);
    }
}