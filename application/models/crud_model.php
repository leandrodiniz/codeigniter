<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model {
	
	//add data in the database
	public function do_insert($data = null)
	{
		if (null != $data) :

			$this->db->insert('user', $data);
			$this->session->set_flashdata('success', 'Registration successful!');
			redirect('home/add');
		
		endif;
	}

	//edit the data of the database
	public function do_update($data = null, $condition = null)
	{
		if (null != $data && null != $condition) :

			$this->db->update('user', $data, $condition);
			$this->session->set_flashdata('success_edit', 'Successfully changed!');
			redirect(current_url());
		
		endif;
	}

	//remove the data of the database
	public function do_delete($condition = null)
	{
		if (null != $condition) :

			$this->db->delete('user', $condition);
			$this->session->set_flashdata('success_remove', 'Successfully excluded!');
			redirect('home/view');
		
		endif;
	}

	//get all users of the database
	public function get_users()
	{
		return $this->db->get('user');
	}

	//get by id
	public function get_byid($id = null)
	{
		if (null != $id) :
			$this->db->where('id', $id);
			$this->db->limit(1);
			return $this->db->get('user');
		else :
			return false;
		endif;
	}

}
